function invert(obj)
{
    let invertedObject={};
    for(key in obj)
    {
        let objkey=key;
        let objValue=obj[key];

        invertedObject[objValue]=key;
    }

    return invertedObject;
}


module.exports=invert;
